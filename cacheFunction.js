function cacheFunction(callback){
  const cacheObj={}
  return function(...args){
    if(cacheObj[args]){
      return cacheObj[args]
    }
    const result=callback(...args)
    cacheObj[args]=result;
    return result;
  }
}


    module.exports =cacheFunction;